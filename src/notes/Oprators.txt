- Important in RxJS

- Provides a lot of useful operators which helps us write clean code and reduce lot of effort in writing custom logic - which leads to many bugs

- An Operators are pure function

- Pure function: a function will also return same value when passed with same input value and has no side effects.

- An (most) operator takes in Observable as input and output will be also an Observable

- Type:
    1. Creation
    2. Mathematical
    3. Join 
    4. Transformation
    5. Filtering
    6. Utility
    7. Conditional
    8. Multicasting
    9. Error handling

Side note:
Initialize variable array of observable ex: variable$: Observable; is a good practice
Operators:
1. Of: 
    - Make observable(can create) from a string, an array or an object.
    - Use it when we want to pass a variable which has to be observable instead of Array or String, use Of operator.
    - Specially useful when working with Models or interfaces.
2. From: 
    - will create an Observable from an array, an array-like object, a promise, an iterable object, or an observable-like object.
    - always take array or array-like
3. FromEvent
    - Create an Observable that emits events of a specific type coming from the given event target.
    - Can bind target elements and methods to make sure we get Observable as output. 